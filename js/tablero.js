(function($) {

/****************MODELO - INI **********************/

  var jugadorModel = Backbone.Model.extend({
    defaults: {
      idJugador: 0,
      idColor: '',
      nombre: ''
    }
  });

  var colorModel = Backbone.Model.extend({
    defaults: {
      idColor: '',
      nombre: '',
      colorWeb:''
    }
  });

  var fichaModel = Backbone.Model.extend({
    defaults: {
      idJugador: 0,
      idColor: '',
      posX:0,
      posY:0,
      ID:""
    },
      idAttribute: "ID"
  });

  TableroModelo = Backbone.Model.extend({
    defaults: {
      filaMax: 8,
      columnasMax: 8,
      numeroJugadores: 2,
      proximoTurno: '',
			tablero:TableroCollection
    }
  });

  

  var TableroCollection = Backbone.Collection.extend({
    model: fichaModel,
    terminado:false,
    initialize: function () {
      console.log('inicializa tablero');
      // This will be called when an item is added. pushed or unshifted
      this.on('add', function(model) {
          console.log('something got added');
      });
      // This will be called when an item is removed, popped or shifted
      this.on('remove',  function(model) {
          console.log('something got removed');
      });
      // This will be called when an item is updated
      this.on('change', function(model) {
          console.log('something got changed:'
          +model.get('posX')+'::'+model.get('posY'));
          //calculamos si existen 4 fichas en raya
          if(!this.terminado){
          var columnas = this.getColumnaByPosX(model.get('posX'));
          var iNumConectados = 0;
          var iNumConectadosMax = 0;

          for (var i = 0,j = 1; i < columnas.length - 1; j++, i++){
            if (columnas[i].get('idColor') ==
             columnas[j].get('idColor')
              && columnas[i].get('idColor')!='' )
              iNumConectados++;
             else{
               if (iNumConectadosMax<iNumConectados)
                  iNumConectadosMax = iNumConectados;
                iNumConectados=0;
             }
          }
          console.log("iNumConectadosMax>>>>"+iNumConectadosMax);
          if (iNumConectadosMax==3){
            this.getMessageSucess('Hay un conecta 4');
            this.terminado = true;
          }

          if (!this.terminado){
            var filas = this.getFilaByPosY(model.get('posY'));

            for (var i = 0,j = 1; i < columnas.length - 1; j++, i++){
              if (filas[i].get('idColor') ==
               filas[j].get('idColor')
                && filas[i].get('idColor')!='' )
                iNumConectados++;
               else{
                 if (iNumConectadosMax<iNumConectados)
                    iNumConectadosMax = iNumConectados;
                  iNumConectados=0;
               }
            }
            console.log("iNumConectadosMax>>>>"+iNumConectadosMax);
            if (iNumConectadosMax==3){            
              this.getMessageSucess('Hay un conecta 4');
              this.terminado = true;
            }
        }
      }else{
        console.log("El juego ha terminado!!!");
      }
      });
    },
    getMessageSucess:((msj) => {
      console.log(msj)
        new PNotify({
            title: 'Premio',
            text: msj,
            type: 'success'
        });
      })
    ,
    getMessageError:((msj) => {
      console.log(msj)
      new PNotify({
          title: 'Error',
          text: msj,
          type: 'error'
      });
    })
    ,
    getColumnaByPosX:function(i){
      var filtrado = this.filter(function (val){
        return val.get("posX") == i;
      });
      return filtrado;
    },
    getFilaByPosY:function(i){
      var filtrado = this.filter(function (val){
        return val.get("posY") == i;
      });
      return filtrado;
    },
    getPrimeraFichaLibreDeLaFila:function(i){
      var fichaLibre = _.find(i
        , function(ficha){
          return ficha.get('idColor')==''
        });

      return fichaLibre;
    }
  });



  var tableroCollection = new TableroCollection();

  var ColoresCollection = Backbone.Collection.extend({
    model: colorModel
  });

  var coloresCollection = new ColoresCollection([
      {
        idColor: '1',
        nombre: 'rojo',
        colorWeb:'#ff0000'
      },
      {
        idColor: '2',
        nombre: 'azul',
        colorWeb:'#0000FF'
      }
  ]);


  var JugadoresCollection = Backbone.Collection.extend({
    model: jugadorModel
  });

  var jugadoresCollection = new JugadoresCollection([
    {
      idJugador: 1,
      idColor: '1',
      nombre: 'Carla'
    },
    {
      idJugador: 2,
      idColor: '2',
      nombre:'Alberto'
    }
  ]);
/****************MODELO - FIN **********************/


  var TableroView = Backbone.View.extend({
		el : '#tablero',
    //instanciamos colecciones que necesitaremos acceder de la vista
    collection: {
      colores: coloresCollection,
      jugadores: jugadoresCollection,
      tablero: tableroCollection
    },
    events: {
      "click tr": "insertaFicha"
    },
    initialize: function() {
      console.log("inicializa tablero");
			this.model	= new TableroModelo();

			var filaMax = this.model.get('filaMax');
			var columnasMax = this.model.get('columnasMax');
			var numeroJugadores = this.model.get('numeroJugadores');

	    console.log("Número máximo de filas::"+filaMax);
			console.log("Número máximo de columnas::"+columnasMax);
			console.log("Número máximo de jugadores::"+numeroJugadores);

      var tablero = this.collection.tablero;
      var cont = 1;
			for (var j = 0; j < columnasMax; ++j){
				for (var i = 0; i < filaMax; ++i){
          //ID será el atributo "ID" del objeto Ficha por el cual si ponemos
          //var ficha = new fichaModel({ID: 1, posX: i, posY:j});,
          //lo estaremos machacando con la misma ficha
          var ficha = new fichaModel({ID: cont, posX: i, posY:j});
          tablero.add(ficha);
          cont++;
          console.log("ficha inicializada ==> posX::"+ficha.get('posX')+" posY:: "+ficha.get('posY'));
        }
			}

      console.log("this.collection.tablero.length>>>"+this.collection.tablero.length);
			//pinta el tablero la primera vez
      this.render();
			//cambia de turno
      this.cambioTurno();
      //cada vez que cambie el modelo asociado es decir
      //TableroModelo con tableroCollection 
      //this.model.bind("change", this.render, this);
      //cuando haya un cambio en la colección tablero se vuelve a enderizar
      //TODO: seria más correcto que salte render cuando cambia el modelo, no la collección y
      //sea la vista que solo vea ese modelo
      this.collection.tablero.bind("change", this.render, this);

    },
    //instanciamos el modelo de la vista
    //model : tableroModelo,
    render: function(){
       console.log("render");
			if ($("#tablero tr").length>0)
				$("#tablero tr").remove();


			var bodyTablero = '';

      var tablero = this.collection.tablero;
      var posY  = '';

      console.log("terminado>>>>"+tablero.terminado);
      //si el juego ya tiene un ganador se deshabilita el evento para seguir añadiendo fichas
      //al tablero , como es el caso de click sobre las filas de la tabla - tablero
      if(tablero.terminado){
        //$(this.el).off('click', 'tr'); se optimiza mejor así
        this.$el.off('click', 'tr');
        console.log('Deshabilitado el evento click sobre las filas del tablero');
      }

      for(var i=0;i < this.model.get('filaMax') ; ++i){
          //vamos a intentar usar filtrado para recuperar
          //las opciones del tablero
          var fichas =tablero.getFilaByPosY(i);
          var td = "";
          var tr = '<tr align="center" datarowid="'+i+'" >';
          for (var j=0;j < fichas.length ; ++j){
            if (fichas[j].get('idColor')=='')
							td = td + '<td bgcolor="#ffffff">X</td>';
            else{
  							//var ficha = new fichaModel();
  							//ficha = fichas[i];

  							console.log("El color es::"+fichas[j].get('idColor'));
  							colorWeb = this.getColorFromIdColor(fichas[j].get('idColor'));

  							td = td + '<td bgcolor="'+colorWeb+'"></td>';
  					}
          }
          tr = tr + td + '</tr>';
          console.log("Número de fichas encontradas :: "+fichas.length +" en la fila ");
          bodyTablero = bodyTablero + tr;
      }

				//this.$el.append(bodyTablero);
			this.$el.html(bodyTablero);
    },
		getColorFromIdColor(idColor){
			var i = 0;
			var colores = this.collection.colores;
			var codigoColor="";
			while (i<colores.models.length){
			 var color =	colores.models[i];
			 var idColorColor = color.get('idColor');
			 if (idColorColor==idColor){
				 codigoColor = color.get('colorWeb');
			 }
			 i++;
			}
			return codigoColor;
		}
    ,insertaFicha: function (event) {
      console.log("insertaFicha");
			 //aquí voy a insertar fichas en el arraytablero
			 //introducir en el tableroArray FichaModel's.
			console.log( "row-id", $(event.currentTarget).attr('datarowid'));
      var i = 0;
			var posFilaInsertaFicha = $(event.currentTarget).attr('datarowid');

			var tablero = this.collection.tablero;
      var filaInsertaFicha =tablero.getFilaByPosY(posFilaInsertaFicha);

      //sacamos la primera ficha libre donde metemos del tablero
      var fichaLibre = tablero.getPrimeraFichaLibreDeLaFila(filaInsertaFicha);
			//var content = filaInsertaFicha[i];
			//creamos la ficha para insertar
			var ficha = new fichaModel();
      ficha = fichaLibre;

			var turnoActual  =  this.model.get('proximoTurno');

			console.log("idJugador::"+turnoActual.get('idJugador')+
		"idColor::"+turnoActual.get('idColor'));

			//agrego una nueva ficha a la fila
			if (fichaLibre!=undefined){
        //populamos la ficha
        fichaLibre.set('idColor',turnoActual.get('idColor'));
        fichaLibre.set('idJugador',turnoActual.get('idJugador'));
				//pinta el tablero
			//	this.render();
				//cambia de turno
				this.cambioTurno();
			}else{
        tableroCollection.getMessageError("fila ya está llena");
			}
    },
		cambiaNombrePorTurno:function(){
			//cambio turno y cambia nombre
			console.log("cambiaNombrePorTurno");

		}
		,cambioTurno:function(){
			 console.log("cambioTurno");
			 //Aquí se cambia el turno al siguiente jugador
			 //En caso de que sea el primer turno se coge el primer jugador

			 var proximoTurno  =  this.model.get('proximoTurno');
			 var jugadorTurno;


			 if (proximoTurno==''){
				 //recoge el primer jugador de jugadoresCollection
				 jugadorTurno = this.collection.jugadores.models[0];
				 console.log("idJugador::"+jugadorTurno.get('idJugador')+" nombre::"+jugadorTurno.get('nombre')
				 +" color ::"+jugadorTurno.get('idColor'));
				 this.model.set('proximoTurno',jugadorTurno);

				 var idColor = this.getColorFromIdColor(jugadorTurno.get('idColor'));

				 $("#nombreJugador").html('<table bgcolor="'+idColor+'" ><tr><td>'+jugadorTurno.get('nombre')+'</td></tr></table>');
			 }  else if  (!this.collection.tablero.terminado)  {
				 //id del jugador que le toca
			 		var idJugadorActual = parseInt(proximoTurno.get('idJugador'));

 					var jugadorModel;
					//inicializo la cuenta
					var i=0;
					var fin = false;

					while (i!=idJugadorActual
						&& i< this.collection.jugadores.models.length
						&& !fin){
						jugadorModel = this.collection.jugadores.models[i];

						if (jugadorModel.get('idJugador')==idJugadorActual){
							j= i+1;
							if (j==this.collection.jugadores.models.length){
								i=0;
								fin=true;
							}
							else {
								i++;
							}
						}else{
							i++;
						}
					}
					//se suma 1 al id del jugador para encontrar
					jugadorModel	=	this.collection.jugadores.models[i];

					var idColor = this.getColorFromIdColor(jugadorModel.get('idColor'));

					$("#nombreJugador").html('<table bgcolor="'+idColor+'" ><tr><td>'+jugadorModel.get('nombre')+'</td></tr></table>');

					this.model.set('proximoTurno',jugadorModel);
			 }else if (this.collection.tablero.terminado){
          $("#mensajeJuego").html('Ya tenemos ganador:');
       }
		}
  });



  new TableroView();

})(jQuery);
